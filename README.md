# VapourSynth VHS FX

This is a port of the "popular" AviSynth VHS effect by [Ukimenustah](https://www.youtube.com/watch?v=osUvH8PRk3g) in VapourSynth, an open-source and cross-platform implementation
of AviSynth.

Based on the modifications by [SMGJohn](https://www.youtube.com/watch?v=IivG8k8oTk4).

**THIS EFFECT HAS NO AUDIO SUPPORT YET!**

## Requirements
* [Python 3.7](https://www.python.org/downloads/)
* [VapourSynth](http://www.vapoursynth.com/)
* [VapourSynth Editor](https://bitbucket.org/mystery_keeper/vapoursynth-editor/downloads/)
* [FFMS2](https://github.com/FFMS/ffms2/releases)
* [ImWri](https://forum.doom9.org/showthread.php?t=170981)
* [FFMPEG](https://ffmpeg.org/)
* [VCR OSD Mono Font](https://www.dafont.com/vcr-osd-mono.font)

**If you are running Windows 7, make sure you have the latest updates, otherwise VapourSynth may fail to run.**

## Installing the Requirements
1. **Install Python.** *(VapourSynth will not install and run without it)*
   * **Windows/Mac**: Download the latest version of Python 3.7 for your OS and architecture. On Windows, if you have a 64-bit system, click on `Windows x86-64 executable installer`. Otherwise (or if unsure), click on `Windows x86 executable installer`.
   * **Linux, BSD**: You may find Python in your distribution's repository. Use your package manager to install it. On Manjaro Linux, for instance, simply install it by passing `sudo pacman -S python`. The package name may differ from distribution to distribution. Otherwise, get the source release from the downloads page, extract it, and run `./configure; make; sudo make install` on the extracted folder.

2. **Install Vapoursynth.**
   * **Windows**: On the VapourSynth home page, click on `Windows binaries`. Get the latest version by clicking on the topmost release and click on `VapourSynth-RXX.exe`. If there isn't any exe file, you may find it in the release below it.
   * **macOS**: See [this thread](https://forum.doom9.org/showthread.php?t=175522). 
   * **Linux, BSD**: VapourSynth may be present in your distribution, either through the official repository or through a PPA/AUR/buildscript. Otherwise, see [here](https://gist.github.com/dreamer2908/f565b9857ee6b2bed3e5) (Instructions are for Ubuntu, similar steps can be taken if you are using another distribution).

3. **Install VapourSynth Editor**
   * **Windows**: On the VapourSynth Editor download page, pick the latest version for your architectiure. These are in 7z format, so you may need to install 7zip. Extract it somewhere easy to find.
   * **macOS**: Get the app from [here](https://bitbucket.org/l33tmeatwad/vapoursynth-editor/downloads/).
   * **Linux, BSD**: VapourSynth Editor may be present in your distribution, either through the official repository or through a PPA/AUR/buildscript. Otherwise, see [here](http://www.l33tmeatwad.com/vapoursynth101/software-setup), under section I, subsection C.

4. **Install FFMS2**
   * **Windows**: Get the latest release from the download page, should be `ffms2-x.xx-msvc.7z`. You may need to install 7zip. Extract the contents of the `x64` or `x86` directory (depending on your architecture) to the VapourSynth plugins directory.
   * **macOS**: See [here](http://macappstore.org/ffms2/).
   * **Linux, BSD**: FFMS2 may be present in your distribution, either through the official repository or through a PPA/AUR/buildscript. Otherwise, run a git clone and then make [here](https://github.com/FFMS/ffms2)

5. Install ImWri (if not present in VapourSynth distribution)
   * **Windows**: Get the files from the forum page, make sure it corresponds to your architecture. You may need to install 7zip. Extract the contents to the VapourSynth plugins directory.

6. **Install FFMPEG**
   * **Windows/Mac**: Download FFMPEG from [here](https://ffmpeg.zeranoe.com/builds/) then extract it somewhere easy to find.
   * **Linux**: FFMPEG may be present in your distribution, either through the official repository or through a PPA/AUR/buildscript. Otherwise, build it from the [source](http://ffmpeg.org/download.html).

7. **Install the VCR OSD Mono Font.**

## Applying the Effect
1. Download the repo [here](https://gitlab.com/bg123/vapoursynth-vhs-fx/-/archive/master/vapoursynth-vhs-fx-master.zip). Extract it somewhere easy to find. The entirety of this repo should be 30MB at most.

2. Open up VapourSynth Editor, then navigate to the folder where you extracted the repo. Open the file named `Effect_NTSC.vpy`.

3. Within VapourSynth Editor, edit the script file.
   * Line 19 should be replaced with the video file you want to apply, eg. `vf = "Your Video File.avi"`. The video file should be put in the repo folder.
   * If you want a camcorder HUD, edit line 22 and select line 41, right click -> `Uncomment Lines`. Otherwise, select line 41 and right click -> `Comment Lines`.

4. Preview the effect by pressing F5.

5. To export the video, go to Script -> `Encode video`. For the executable, navigate to the folder where you extracted FFMPEG, go to the `bin` directory and choose `ffmpeg` (or just type in `ffmpeg` on Linux). Make sure the Header is set to `Y4M`. In the Arguments box, type in `-i - `, followed by the full path of your output video file (in any format). To save the video as `test.mp4` on the desktop, for instance, you need to type in `-i - C:\Users\User\Desktop\test.mp4`