from vapoursynth import core, YUV420P8, RGB, YUV, GRAY
import functools
import math

# ========== Definitions ==========
fix_fps_num = 30000
fix_fps_den = 1001
# 30000 / 1001 = 29,97... fps

# font directory relative to script
font_dir = "../"
# HUD styles, in the Advanced SubStation Alpha subtitle format
hud_style = ["VCR OSD Mono,36,&HDDDDDD,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.5,0,7,28,0,28,1",
             "VCR OSD Mono,36,&HDDDDDD,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.5,0,7,70,0,64,1",
             "VCR OSD Mono,36,&HDDDDDD,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.5,0,8,0,0,28,1",
             "VCR OSD Mono,32,&HFFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,55,100,0,0,1,1,0,1,30,0,23,1"]

# Video file
vf = "sample.m4v"

# Custom date for camcorder HUD
vd = "AM 07:30"+"\\N " \
    +"APR. 20 1989"

# ========== FPS conversion functions from http://forum.doom9.org/showthread.php?t=171417 ==========
def frame_adjuster(n, clip, target_fps_num, target_fps_den):
    real_n = math.floor(n / (target_fps_num / target_fps_den * clip.fps_den / clip.fps_num))
    one_frame_clip = clip[real_n] * (len(clip) + 100)
    return one_frame_clip

def adjust_video(clip, target_fps_num, target_fps_den):
    attribute_clip = core.std.BlankClip(clip, length=math.floor(len(clip) * target_fps_num / target_fps_den * clip.fps_den / clip.fps_num), fpsnum=target_fps_num, fpsden=target_fps_den)
    adjusted_clip = core.std.FrameEval(attribute_clip, functools.partial(frame_adjuster, clip=clip, target_fps_num=target_fps_num, target_fps_den=target_fps_den))
    return adjusted_clip

# ==================== Ingredients ====================
source = core.ffms2.Source(vf)
source = adjust_video(source, fix_fps_num, fix_fps_den).resize.Bilinear(392,480)

# If you want a camcorder HUD, uncomment the line below this
source = core.sub.Subtitle(source, vd, end=160, fontdir=font_dir, style=hud_style[3])

loss   = core.imwri.Read("mask/hsync_%03d.bmp").std.AssumeFPS(fpsnum=fix_fps_num,fpsden=fix_fps_den).std.Loop(0)
noise  = core.ffms2.Source("noise-preprocess.avi").std.AssumeFPS(fpsnum=fix_fps_num,fpsden=fix_fps_den).resize.Bilinear(400,480).std.Loop(0)

# ==================== Signal processing ====================
# Luminance
grey_1 = core.std.ShufflePlanes(clips=source, planes=0, colorfamily=GRAY).resize.Bilinear(288,480).resize.Bicubic(400,480)
grey_2 = core.resize.Point(grey_1,800,480).std.CropAbs(left=0,top=0,width=799,height=480).std.AddBorders(left=2,top=0,right=0,bottom=0).resize.Bilinear(400,480)
loss_g = core.std.ShufflePlanes(loss, planes=1, colorfamily=GRAY).resize.Point(400,480)
grey_sync_1 = core.std.PreMultiply(grey_1,loss_g)
grey_sync_2 = core.std.PreMultiply(grey_2,core.std.Invert(loss_g))
grey = core.std.Expr(clips=[grey_sync_1, grey_sync_2], expr=["x y + 1.15 /"]).resize.Bilinear(format=YUV420P8)

# Chroma
color_in_u = core.resize.Bilinear(source,100,480).resize.Bilinear(400,480).std.BoxBlur(hradius=4,vradius=2).std.AddBorders(left=4,top=2,right=0,bottom=0).std.CropAbs(left=0,top=0,width=400,height=480).std.BoxBlur(hradius=1,vradius=2)
color_in_v = core.resize.Bilinear(color_in_u,format=YUV420P8)
loss_r = core.std.ShufflePlanes(loss, planes=1, colorfamily=GRAY).resize.Point(400,480).std.Expr("x 0.65 *")
color_u = core.std.PreMultiply(color_in_u,loss_r).resize.Bilinear(format=YUV420P8)
color_v = core.std.PreMultiply(color_in_v,core.std.Trim(loss_r,first=32)).resize.Bilinear(format=YUV420P8).std.Expr("x 1 *")
color =  core.std.ShufflePlanes(clips=[color_u,color_u,color_v], planes=[0,1,2], colorfamily=YUV)
color = core.std.Expr(clips=[color, color_in_v],expr=["","x 0.5 * y + 1.45 /","x 0.5 * y + 1.45 /"])

# ==================== Compositing signal ====================
final_signal = core.std.ShufflePlanes(clips=[grey,color,color], planes=[0,1,2], colorfamily=YUV)
final_video = core.std.Expr(clips=[final_signal, noise], expr=["y 0.45 * x +", "", ""])
final_video = core.std.Levels(final_video,min_in=36,gamma=1.0,max_in=255,min_out=0,max_out=255,planes=[0])
final_video = core.resize.Bilinear(final_video, 640,480)

final = final_video

# ==================== VCR player display ====================
player = core.std.BlankClip(length=60, color=[66,66,221], fpsnum=fix_fps_num, fpsden=fix_fps_den).resize.Bicubic(format=YUV420P8, matrix_s="709")

out = player+final

out = core.sub.Subtitle(out, "> PLAY", start=10, end=160, fontdir=font_dir, style=hud_style[0])
out = core.sub.Subtitle(out, "LP NTSC", start=70, end=160, fontdir=font_dir, style=hud_style[1])
#out = core.sub.Subtitle(out, "[HI-FI]", start=180, end=330, fontdir=font_dir, style=hud_style[2])

# ==================== Output ====================
out.set_output()